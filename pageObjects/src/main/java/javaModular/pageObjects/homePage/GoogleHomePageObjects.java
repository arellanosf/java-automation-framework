package javaModular.pageObjects.homePage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleHomePageObjects {

    @FindBy(xpath = "//input[@name='q']")
    public WebElement txt_searchBox;

    @FindBy(xpath = "(//input[@name='btnK'])[1]")
    public WebElement submit_searchButton;
}