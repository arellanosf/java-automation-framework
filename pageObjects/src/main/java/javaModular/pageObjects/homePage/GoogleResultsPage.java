package javaModular.pageObjects.homePage;

import javaModular.framework.core.BasePage;
import org.openqa.selenium.WebDriver;

public class GoogleResultsPage extends BasePage {

    private GoogleResultsPageObjects googleResultsPageObjects = (GoogleResultsPageObjects) super.InitializedPage;

    public GoogleResultsPage(WebDriver driver) {
        super(driver, GoogleResultsPageObjects.class);
    }

    public void selectSearchResult() {
        doClick(googleResultsPageObjects.link_resultToClick);
    }
}
