package javaModular.pageObjects.homePage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleResultsPageObjects {

    @FindBy(xpath = "//a/h3[contains(text(),'Hola - Free VPN, Secure Browsing, Unrestricted Access')]")
    public WebElement link_resultToClick;
}
