package javaModular.pageObjects.homePage;

import javaModular.framework.core.BasePage;
import org.openqa.selenium.WebDriver;

public class GoogleHomePage extends BasePage {

    private GoogleHomePageObjects googleHomePageObjects = (GoogleHomePageObjects) super.InitializedPage;

    public GoogleHomePage(WebDriver driver) {
        super(driver, GoogleHomePageObjects.class);
    }

    public void googleSearch() {
        enterTextOnInput(googleHomePageObjects.txt_searchBox, "hola");
        doClick(googleHomePageObjects.submit_searchButton);
    }
}
