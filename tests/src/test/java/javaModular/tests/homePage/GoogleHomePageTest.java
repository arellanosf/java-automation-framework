package javaModular.tests.homePage;

import javaModular.pageObjects.homePage.GoogleResultsPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import javaModular.framework.core.BaseTest;
import javaModular.pageObjects.homePage.GoogleHomePage;

public class GoogleHomePageTest extends BaseTest {

    private WebDriver driver = getDriver();

    @Test
    public void homepageTest() throws InterruptedException {
        GoogleHomePage googleHomePage = new GoogleHomePage(driver);
        googleHomePage.googleSearch();
        Thread.sleep(3000);

        GoogleResultsPage googleResultsPage = new GoogleResultsPage(driver);
        googleResultsPage.selectSearchResult();
        Thread.sleep(3000);
    }
}
