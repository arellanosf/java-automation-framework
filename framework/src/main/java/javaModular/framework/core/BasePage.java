package javaModular.framework.core;

import org.omg.CORBA.TIMEOUT;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    protected WebDriver driver;
    private WebDriverWait wait;
    protected Object InitializedPage;

    private static final int TIMEOUT = 5;

    public BasePage(WebDriver driver, Class targetClass) {
        this.driver = driver;
        wait = new WebDriverWait(driver, TIMEOUT);
        InitializedPage = PageFactory.initElements(driver, targetClass);
    }

    protected void enterTextOnInput(WebElement element, String text) {
        wait.until(ExpectedConditions.visibilityOf(element)).sendKeys(text);
    }

    protected void doClick(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element)).click();
    }
}
