package javaModular.framework.core;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BaseTest {

    private WebDriver driver;

    protected BaseTest() {
        ChromeDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @BeforeSuite
    public void beforeSuite() {
        driver.manage().window().maximize();
        driver.navigate().to("https://www.google.com");
    }

    @AfterSuite
    public void afterSuite() {
        if (null != driver) {
            driver.close();
            driver.quit();
        }
    }

    protected WebDriver getDriver() {
        return this.driver;
    }
}
